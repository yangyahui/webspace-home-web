/*
 * @Author: yangyahui
 * @Date: 2022-08-08 18:24:15
 * @LastEditors: yangyahui
 * @LastEditTime: 2022-08-18 10:55:13
 * @Description: 请填写简介
 */
import request from 'umi-request';

export const fetchCardsList = async (params) => {
    return request('/api/wsp/getCardsList', {
        method: 'get',
        params
    })
}
export const fetchThemeList = async (params) => {
    return request('/api/wsp/getClassifyList', {
        method: 'get',
        params
    })
}
export const createCard = async (data) => {
    return request('/api/wsp/createCard', {
        method: 'post',
        data
    })
}
export const updateCard = async (data) => {
    return request('/api/wsp/updateCard', {
        method: 'post',
        data
    })
}
export const createTheme = async (data) => {
    return request('/api/wsp/createClassify', {
        method: 'post',
        data
    })
}
export const updateTheme = async (data) => {
    return request('/api/wsp/updateClassify', {
        method: 'post',
        data
    })
}
export const deleteCard = async (data) => {
    return request('/api/wsp/deleteCard', {
        method: 'post',
        data
    })
}
export const deleteTheme = async (data) => {
    return request('/api/wsp/deleteClassify', {
        method: 'post',
        data
    })
}
export const fetchPictureList = async (params) => {
    return request('/api/wsp/getFiles', {
        method: 'get',
        params
    })
}