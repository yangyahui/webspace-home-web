import React from 'react'
import { Alert, notification, Typography, Drawer, Image, Space, Form, Button, Col, Row, Input, Select, Radio, Upload, message, Divider, Tooltip, Card, Avatar } from 'antd';
import { LoadingOutlined, CheckOutlined, CloseOutlined, EditOutlined, PlusOutlined, DeleteOutlined, LinkOutlined, HighlightOutlined } from '@ant-design/icons';
import { fetchPictureList, updateTheme, fetchThemeList, createCard, updateCard, deleteCard, deleteTheme, createTheme } from 'service/card'
import './DrawerForm.less'
import "@gabrielfins/ripple-effect";

const { Option } = Select;
const { Meta } = Card;
const { Text, Link } = Typography;
const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}
const beforeUpload = (file) => {
    const isJpgOrPngOrWebp = ['image/jpeg', 'image/png', 'image/webp'].includes(file.type);
    if (!isJpgOrPngOrWebp) {
        message.error('You can only upload JPG/PNG/WEBP file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPngOrWebp && isLt2M;
}
class DrawerForm extends React.Component {
    state = {
        visible: true,
        loading: false,
        createThemeloading: false,
        imageUrl: '',
        themeList: [{ _id: '31313', name: '科研服务' }],
        cardsList: [],
        theme: '',
        childrenDrawer: false,
        classifyId: '',
        logoHref: '',
        desc: '',
        cardName: '',
        pictureList: [],
        logoHrefFromLib: '',
        editableStr: '',
        modifiedClassifyName: ''
    };
    componentDidMount() {
        this.refreshThemeList()
    }
    formRef = React.createRef();
    componentWillReceiveProps(nextProps) {
        /**
         * 这里可以继续优化，减少表单不必要的setFieldsValue
         */
        const isSameProps = JSON.stringify(nextProps) == JSON.stringify(this.props)
        debugger
        if (!isSameProps) {
            debugger
            const { isCreate, isCreatedByCopied, initialValues } = nextProps
            if (!isCreate || isCreatedByCopied) {
                debugger
                if (initialValues && this.formRef.current) {
                    this.formRef.current.setFieldsValue(initialValues)
                    const { classifyId, logoFetchWay, logoHref, name: cardName, desc } = initialValues
                    this.setState({ classifyId })
                    this.setState({ cardName })
                    this.setState({ desc })
                    this.setState({ logoHref })
                    if (logoFetchWay === 2) {
                        this.setState({
                            imageUrl: logoHref,
                            loading: false,
                        })
                    } else if (logoFetchWay === 3) {
                        this.setState({ logoHrefFromLib: logoHref })
                    }
                }
            } else {
                debugger
                if (this.formRef.current) {
                    this.setState({ imageUrl: '' })
                    this.setState({ logoHrefFromLib: '' })
                    this.setState({ cardName: '' })
                    this.setState({ desc: '' })
                    this.setState({ logoHref: '' })
                    this.formRef.current.resetFields()
                }
            }
        }
    }
    openNotificationWithIcon = (type, message, description) => {
        notification[type]({
            message,
            description,
            style: {
                width: 350,
            },
        });
    };
    refreshThemeList = async (action) => {
        const res = await fetchThemeList()
        const { code, data } = res
        if (code === 200) {
            data.map(theme => { theme.editStatus = false; theme.showDeleteDialog = false })
            this.setState({ themeList: data }, () => {
                if (action === 'submitEditTheme') {
                    this.props.refreshCardsList()
                }
            })
        }
    }
    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    loading: false,
                }),
            );
        }
    };

    onNameChange = event => {
        this.setState({
            theme: event.target.value,
        });
    };
    addItem = async () => {
        this.setState({ createThemeloading: true })
        const { theme } = this.state;
        const res = await createTheme({ name: theme })
        const { code } = res
        if (code === 200) {
            const response = await fetchThemeList()
            const { code, data } = response
            if (code === 200) {
                this.setState({ themeList: data, theme: '', createThemeloading: false }, () => {
                    const [box] = document.getElementsByClassName('rc-virtual-list-holder')
                    box.scrollTop = box.scrollHeight
                })
            }
        }
    };
    onClickDelete = async (e, _id) => {
        // debugger
        e.stopPropagation()
        const { themeList } = this.state
        themeList.map(theme => {
            theme.editStatus = false
            if (theme._id === _id) {
                theme.showDeleteDialog = true
            } else {
                theme.showDeleteDialog = false
            }
        })
        this.setState({ themeList })
    };
    deleteTheme = async (e, _id, name) => {
        e.stopPropagation()
        const res = await deleteTheme({ _id })
        const { code } = res
        if (code === 200) {
            // this.openNotificationWithIcon('success', `分类-${name} 删除成功`, '该分类下的卡片已转移到默认分类')
            this.refreshThemeList()
            this.props.refreshCardsList()
        }
    };
    cancelDeleteTheme = (e, _id) => {
        e.stopPropagation()
        const { themeList } = this.state
        themeList.map(theme => {
            if (_id === theme._id) {
                theme.showDeleteDialog = false
            }
        })
        this.setState({ themeList })
    }
    editTheme = async (e, _id, name) => {
        e.stopPropagation()
        this.setState({ modifiedClassifyName: name })
        const { themeList } = this.state
        themeList.map(theme => {
            theme.showDeleteDialog = false
            if (_id === theme._id) {
                theme.editStatus = true
            } else {
                theme.editStatus = false
            }
        })
        this.setState({ themeList })
    };
    submitEditTheme = async (e, _id) => {
        e.stopPropagation()
        const { modifiedClassifyName: name } = this.state
        const res = await updateTheme({ _id, name })
        if (res && res.code === 200) {
            // this.refreshThemeList('submitEditTheme')
            const res = await fetchThemeList()
            const { code, data } = res
            if (code === 200) {
                data.map(theme => { theme.editStatus = false })
                this.setState({ themeList: data }
                )
                this.props.refreshCardsList()
            }
        }
    }
    cancelEdit = (e, _id) => {
        e.stopPropagation()
        const { themeList } = this.state
        themeList.map(theme => {
            if (_id === theme._id) {
                theme.editStatus = false
            }
        })
        this.setState({ themeList })
    }
    deleteCardById = async () => {
        const { initialValues: { _id } } = this.props
        const res = await deleteCard({ _id })
        const { code } = res
        if (code === 200) {
            message.success('删除成功');
            this.props.refreshCardsList()
            this.props.setDrawShow(false)
        }
    }
    onFinish = async (isContinue) => {
        // this.formRef.current.setFieldsValue({ classifyId: this.state.classifyId })
        // debugger
        const validateResult = await this.formRef.current.validateFields()
        if (validateResult) {
            const values = this.formRef.current.getFieldsValue()
            debugger
            const { classifyId, logoHrefFromLib } = this.state
            const { isCreate, isCreatedByCopied, initialValues: { _id } } = this.props
            const { logoFetchWay, name, href, desc, logoHref, logoHrefUpload } = values
            let data = {}
            if (logoFetchWay === 1) {
                data = {
                    name, href, desc, logoHref, classifyId
                }
            } else if (logoFetchWay === 2) {
                if (logoHrefUpload) {
                    const { file: { response } } = logoHrefUpload
                    if (response) {
                        const [{ code = 200, url: logoHref }] = response
                        if (code === 200) {
                            data = {
                                name, href, desc, logoHref, classifyId
                            }
                        } else {
                            data = {
                                name, href, desc, logoHref: '', classifyId
                            }
                        }
                    }
                } else {
                    data = {
                        name, href, desc, logoHref: isCreatedByCopied ? this.state.logoHref : logoHref, classifyId
                    }
                }
            } else {
                data = {
                    name, href, desc, logoHref: logoHrefFromLib, classifyId
                }
            }
            if (isCreate || isCreatedByCopied) {
                const res = await createCard({ ...data, logoFetchWay })
                const { code } = res
                if (code === 200) {
                    message.success('添加成功');
                    this.props.refreshCardsList()
                    if (!isContinue) {
                        this.props.setDrawShow(false)
                    }
                }
            } else {
                const res = await updateCard({ ...data, logoFetchWay, _id })
                const { code } = res
                if (code === 200) {
                    message.success('修改成功');
                    this.props.refreshCardsList()
                    this.props.setDrawShow(false)
                }
            }

        }
    };
    showChildrenDrawer = () => {
        this.setState({ childrenDrawer: true })
    };
    onChildrenDrawerClose = () => {
        this.setState({ childrenDrawer: false })
    };
    openPictureList = async () => {
        const res = await fetchPictureList()
        const { data: pictureList } = res
        this.setState({ pictureList })
        this.showChildrenDrawer()
    };
    onThemechange = (classifyId) => {
        this.setState({ classifyId })
        this.formRef.current.setFieldsValue({ classifyId })
        const { themeList } = this.state
        themeList.map(theme => {
            theme.showDeleteDialog = false
            theme.editStatus = false
        })
        this.setState({ themeList })
    }
    onValuesChange = (changeValue) => {
        if ('logoHref' in changeValue) {
            this.setState(changeValue)
        } else if ('logoHrefUpload' in changeValue) {
            const { file: { response } } = changeValue['logoHrefUpload']
            if (response) {
                const [{ url: logoHref }] = response
                this.setState({ logoHref })
            }
        } else if ('desc' in changeValue) {
            this.setState(changeValue)
        } else if ('name' in changeValue) {
            this.setState({ cardName: changeValue['name'] })
        }
    }
    onPictureSelected = (picture) => {
        this.setState({ logoHrefFromLib: picture })
        this.setState({ logoHref: picture })
        this.setState({ childrenDrawer: false })
    }
    render() {
        const { loading, logoHrefFromLib, pictureList, createThemeloading, imageUrl, themeList, theme, logoHref, desc, cardName } = this.state;
        const { visible, setDrawShow, isCreate, initialValues } = this.props
        const uploadButton = (
            <div>
                {loading ? <LoadingOutlined /> : <PlusOutlined />}
                <div style={{ marginTop: 8 }}>上传</div>
            </div>
        );
        return (
            <>
                <Drawer
                    title={isCreate ? '新建卡片' : `编辑卡片-${initialValues.name}`}
                    width={400}
                    closable={false}
                    forceRender={true}
                    onClose={() => setDrawShow(false)}
                    visible={visible}
                    destroyOnClose={true}
                    bodyStyle={{ paddingBottom: 80 }}
                    footer={
                        <div
                            style={{
                                textAlign: 'right',
                            }}
                        >
                            <Button className='md-ripples' onClick={() => setDrawShow(false)} style={{ marginRight: 8 }}>
                                取消
                            </Button>
                            <Button type="primary" className='md-ripples' onClick={isCreate ? this.onFinish.bind(this, true) : this.deleteCardById} style={{ marginRight: 8, border: '1px solid transparent', background: isCreate ? 'green' : '' }} danger={!isCreate}>
                                {isCreate ? '添加并继续' : '删除'}
                            </Button>
                            <Button className='md-ripples' type="primary" onClick={this.onFinish.bind(this, false)}>
                                确定
                            </Button>
                        </div>
                    }
                >
                    <Form layout="vertical" onValuesChange={this.onValuesChange} hideRequiredMark ref={this.formRef} initialValues={{ logoFetchWay: 1 }} onFinish={this.onFinish}>
                        <Card
                            className='md-ripples'
                            style={{ width: 244, margin: '0 auto 20px', boxShadow: '0 0 10px 2px rgba(0,0,0,0.4)', border: 'none' }}
                        >
                            <Meta
                                avatar={logoHref ? <Avatar shape="square" src={logoHref} /> : <Avatar shape="square" style={{ backgroundColor: 'rgba(53, 115, 185, 1)' }}>{cardName.slice(0, 1).toUpperCase() || 'logo'}</Avatar>}
                                title={cardName || "卡片预览样式"}
                                description={desc || "This is the description"}
                            />
                        </Card>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item
                                    name="name"
                                    label="名称"
                                    rules={[{ required: true, message: 'Please enter user name' }]}
                                >
                                    <Input
                                        allowClear
                                        prefix={<HighlightOutlined />}
                                        showCount maxLength={10}
                                        placeholder="Please enter user name"
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    name="href"
                                    label="导航链接"
                                    rules={[{ required: true, message: 'Please enter url' }]}
                                >
                                    <Input
                                        allowClear
                                        style={{ width: '100%' }}
                                        prefix={<LinkOutlined />}
                                        placeholder="Please enter url"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item
                                    name="logoFetchWay"
                                    label="logo"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'please choose logo fetch way',
                                        },
                                    ]}
                                >
                                    <Radio.Group>
                                        <Radio value={1}>图片外链</Radio>
                                        <Radio value={2}>自定义上传</Radio>
                                        <Radio value={3}>从图库中选择</Radio>
                                    </Radio.Group>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item
                                    noStyle
                                    shouldUpdate={(prevValues, currentValues) => prevValues.logoFetchWay !== currentValues.logoFetchWay}
                                >
                                    {({ getFieldValue }) =>
                                        getFieldValue('logoFetchWay') === 1 ? (
                                            <Form.Item name="logoHref">
                                                <Input
                                                    prefix={<LinkOutlined />}
                                                    allowClear
                                                    placeholder="Please enter url"
                                                />
                                            </Form.Item>
                                        ) : getFieldValue('logoFetchWay') === 2 ? (<Form.Item name="logoHrefUpload">
                                            <Upload
                                                name="file"
                                                listType="picture-card"
                                                className="avatar-uploader"
                                                showUploadList={false}
                                                action="/api/wsp/upload"
                                                beforeUpload={beforeUpload}
                                                onChange={this.handleChange}
                                            >
                                                {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                                            </Upload>
                                        </Form.Item>) : <Form.Item><Image
                                            // width={78}
                                            height={78}
                                            src={logoHrefFromLib}
                                            style={{ cursor: 'pointer' }}
                                            onClick={this.openPictureList}
                                            preview={false}
                                            fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                                        />
                                        </Form.Item>
                                    }
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            {/* <Col span={24}>
                                <Alert
                                    message="分类被删除后,里面的卡片会被转移到默认分类，如需找回可重新编辑卡片分类"
                                    type="warning"
                                    closable
                                    showIcon
                                    style={{
                                        marginBottom: '10px'
                                    }}
                                />
                            </Col> */}
                            <Col span={24}>
                                <Form.Item
                                    name="classifyId"
                                    label={
                                        <div>
                                            <p>分类</p>
                                            <Alert
                                                message="分类被删除后,里面的卡片会被转移到默认分类，如需找回可重新编辑卡片分类"
                                                type="warning"
                                                closable
                                                showIcon
                                                style={{
                                                    marginBottom: '10px'
                                                }}
                                            />
                                        </div>}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'please choose or create topic',
                                        },
                                    ]}
                                >
                                    <Select
                                        style={{ width: '100%' }}
                                        placeholder="please choose or create topic"
                                        optionLabelProp="label"
                                        listHeight={200}
                                        onChange={this.onThemechange}
                                        virtual={false}
                                        // defaultOpen={true}
                                        // open={true}
                                        // getPopupContainer={() => document.getElementById('area')}
                                        notFoundContent={'暂无数据，请点击下方新建'}
                                        dropdownRender={menu => (
                                            <div>
                                                {menu}
                                                <Divider style={{ margin: '4px 0' }} />
                                                <div style={{ display: 'flex', flexWrap: 'nowrap', padding: 8 }}>
                                                    <Input style={{ flex: 'auto' }} value={theme} onChange={this.onNameChange} />
                                                    <Button type="link"
                                                        style={{ flex: 'none', padding: '8px', display: 'block', cursor: 'pointer' }}
                                                        onClick={this.addItem}
                                                        loading={createThemeloading}
                                                        disabled={!theme}
                                                    >
                                                        <PlusOutlined /> 新建分类
                                                    </Button>
                                                </div>
                                            </div>
                                        )}
                                    >
                                        {themeList.map(item => (
                                            <Option className='floatOption' key={item._id} label={item.name}>
                                                <div className='flexSelect'>
                                                    {item.editStatus ?
                                                        <Input autoFocus={true} onChange={(e) => { this.setState({ modifiedClassifyName: e.target.value }) }} onBlur={(e) => { this.cancelEdit(e, item._id) }} style={{ marginRight: '10px' }} size="middle" onClick={(e) => { e.stopPropagation() }} showCount defaultValue={item.name} maxLength={20} />
                                                        : item.showDeleteDialog ?
                                                            <p style={{ lineHeight: '36px', marginBottom: 0 }}>
                                                                <span>确定
                                                                    <Text strong type="danger">删除</Text>
                                                                    分类-
                                                                    <Text
                                                                        italic
                                                                        style={
                                                                            {
                                                                                width: 120,
                                                                            }
                                                                        }
                                                                        ellipsis={
                                                                            {
                                                                                tooltip: item.name,
                                                                            }
                                                                        }
                                                                    >
                                                                        {item.name}？
                                                                    </Text>
                                                                </span>
                                                            </p>
                                                            : <Text
                                                                type={item.name === '默认分类' ? "secondary" : 'default'}
                                                                style={
                                                                    {
                                                                        width: 200,
                                                                    }
                                                                }
                                                                ellipsis={
                                                                    {
                                                                        tooltip: item.name,
                                                                    }
                                                                }
                                                            >
                                                                {item.name}
                                                            </Text>}
                                                    {item.name !== '默认分类' &&
                                                        (
                                                            item.showDeleteDialog ?
                                                                <Space>
                                                                    <Button danger shape="square" size="middle" style={{ fontSize: '12px' }} onClick={(e) => { this.deleteTheme(e, item._id, item.name) }} >是</Button>
                                                                    <Button shape="square" size="middle" style={{ fontSize: '12px' }} onClick={(e) => { this.cancelDeleteTheme(e, item._id) }} >否</Button>
                                                                </Space>
                                                                : <Space>
                                                                    <Button shape="square" size={item.editStatus ? "middle" : "small"} onClick={(e) => { item.editStatus ? this.submitEditTheme(e, item._id) : this.editTheme(e, item._id, item.name) }} icon={item.editStatus ? <CheckOutlined /> : <EditOutlined />} />
                                                                    <Button shape="square" size={item.editStatus ? "middle" : "small"} onClick={(e) => { item.editStatus ? this.cancelEdit(e, item._id) : this.onClickDelete(e, item._id) }} icon={item.editStatus ? <CloseOutlined /> : <DeleteOutlined />} />
                                                                </Space>
                                                        )
                                                    }
                                                </div>
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={24}>
                                <Form.Item
                                    name="desc"
                                    label="描述"
                                >
                                    <Input
                                        allowClear
                                        prefix={<HighlightOutlined />}
                                        showCount maxLength={10}
                                        placeholder="please enter url description"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                    {/* 子抽屉 */}
                    <Drawer
                        title="图标库"
                        width={320}
                        closable={false}
                        onClose={this.onChildrenDrawerClose}
                        visible={this.state.childrenDrawer}
                    >
                        {/* This is two-level drawer */}
                        <Space size={[8, 8]} wrap >
                            {pictureList.map((picture, key) => <Image
                                width={61}
                                key={key}
                                className='md-ripples'
                                preview={false}
                                onClick={this.onPictureSelected.bind(this, picture)}
                                style={{ cursor: 'pointer', background: 'white' }}
                                src={picture}
                            />)}
                        </Space>
                    </Drawer>
                </Drawer>
            </>
        )
    }

}
export default DrawerForm