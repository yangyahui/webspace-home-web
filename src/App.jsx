/*
 * @Author: yangyahui
 * @Date: 2022-08-08 18:33:21
 * @LastEditors: yangyahui
 * @LastEditTime: 2022-08-16 00:58:08
 * @Description: 请填写简介
 */
import React, { useEffect, useState } from 'react'
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;
import { HomeOutlined, AppstoreOutlined } from '@ant-design/icons';
import Home from './pages/Home';
import './App.less'
import 'antd/dist/antd.less';

const menuItems = [
  {
    key: 'main',
    icon: <HomeOutlined />,
    label: '开发主页入口'
  },
  {
    key: 'operation',
    icon: <AppstoreOutlined />,
    label: '操作手册'
  }
]
class App extends React.Component {
  state = {
    current: 'main',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({ current: e.key });
  };

  render() {
    const { current } = this.state;
    return (
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
          <div className="logo" />
          <Menu items={menuItems}  onClick={this.handleClick} theme="dark" selectedKeys={[current]} mode="horizontal">
          </Menu>
        </Header>
        <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            {/* <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item> */}
          </Breadcrumb>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
            <Home />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Web Develop Tools Page ©2022 Created by YangYaHui</Footer>
      </Layout>
    );
  }
}

export default App
