/*
 * @Author: yangyahui
 * @Date: 2022-08-08 18:33:21
 * @LastEditors: yangyahui
 * @LastEditTime: 2022-08-22 12:51:27
 * @Description: 请填写简介
 */
import React, { useEffect, useState } from 'react'
import { Dropdown, Button, Menu, Card, Avatar, Divider, Space, message } from 'antd';
//import { Drawer, Form, Button, Col, Row, Input, Select, DatePicker } from 'antd';
//import { EditOutlined, EllipsisOutlined, SettingOutlined, PlusOutlined } from '@ant-design/icons';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import NavigationIcon from '@material-ui/icons/Navigation';
import "@gabrielfins/ripple-effect";
import './index.less'
import 'antd/dist/antd.less';
import DrawerForm from './components/DrawerForm'
import { fetchCardsList, deleteCard } from 'service/card'

const useStyles = makeStyles((theme) => ({
    operation: {
        '& > *': {
            margin: theme.spacing(1),
        },
        position: 'fixed',
        right: '70px',
        bottom: '100px',
        zIndex: 1000

    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));
const arr = []
for (let i = 0; i < 20; i++) {
    arr.push(i)
}

const { Meta } = Card;
const Home = (props) => {
    const [cardsList, setCardsList] = useState([])
    const [visible, setVisible] = useState(false)
    const [initialValues, setInitialValues] = useState({ logoFetchWay: 1 })
    const [isCreate, setIsCreate] = useState(true)
    const [isAdminMethod, setAdminMethod] = useState(false)
    const [isCreatedByCopied, setIsCreatedByCopied] = useState(false)
    const [isFirstMounted,setIsFirstMounted] = useState(true)
    const refreshCardsList = async () => {
        const res = await fetchCardsList()
        const { code, data } = res
        if (code === 200) {
            setCardsList(data)
        }
    }
    const setDrawShow = (visible) => { setVisible(visible) }
    useEffect(() => {
        if (window.location.href.includes('admin')) {
            setAdminMethod(true)
        } else {
            setAdminMethod(false)
        }
        refreshCardsList()
    }, [])
    const classes = useStyles();
    const editCard = (card) => {
        setInitialValues(card)
        setIsCreatedByCopied(false)
        setIsCreate(false)
        setDrawShow(true)
        setIsFirstMounted(false)
        // debugger
    }
    const copyCard = (card) => {
        setInitialValues(card)
        setIsCreate(true)
        setIsCreatedByCopied(true)
        setDrawShow(true)
        // debugger
    }
    const deleteCardById = async (card) => {
        const { _id } = card
        const res = await deleteCard({ _id })
        const { code } = res
        if (code === 200) {
            message.success('删除成功');
            refreshCardsList()
            setDrawShow(false)
        }
    }
    const menu = (card) => <Menu items={[
        {
            key: '1',
            label: (
                <Button size="small" type="link" onClick={(e) => { e.preventDefault(); editCard(card) }}>编辑</Button>
            ),
        },
        {
            key: '2',
            label: (
                <Button size="small" type="link" onClick={(e) => { e.preventDefault(); copyCard(card) }}>复制</Button>
            ),
        },
        {
            key: '3',
            label: (
                <Button size="small" type="link" onClick={(e) => { e.preventDefault(); deleteCardById(card) }}>删除</Button>
            ),
        }
    ]} />
    return (
        <>
            <div className={classes.operation}>
                <Fab color="primary" aria-label="add" onClick={() => { setIsCreate(true); setDrawShow(true);if(isFirstMounted){setIsFirstMounted(false)} }}>
                    <AddIcon />
                </Fab>
                {/* <Fab color="secondary" aria-label="edit">
                    <EditIcon />
                </Fab>
                <Fab variant="extended">
                    <NavigationIcon className={classes.extendedIcon} />
                    Navigate
                </Fab> */}
            </div>
            <DrawerForm isCreatedByCopied={isCreatedByCopied} isCreate={isCreate} initialValues={initialValues} visible={visible} setDrawShow={setDrawShow} refreshCardsList={refreshCardsList} />
            {
                cardsList.map((classify, i) => <div key={i}>
                    <Divider orientation="left">{classify.classifyName}</Divider>
                    <Space size={[8, 16]} wrap>
                        {
                            classify.cards.map((card, j) => <a key={j} href={card.href} target="_blank">
                                <Card
                                    className='md-ripples'
                                    style={{ width: 229 }}
                                >
                                    <Meta
                                        avatar={card.logoHref ? <Avatar alt='ee' shape="square" src={card.logoHref} /> : <Avatar shape="square" style={{ backgroundColor: 'rgba(53, 115, 185, 1)' }}>{card.name.slice(0, 1).toUpperCase()}</Avatar>}
                                        title={card.name}
                                        className='card-name'
                                        description={card.desc || '暂未描述'}
                                    />
                                    {isAdminMethod && <Dropdown trigger={['click']} overlay={menu(card)}>
                                        {<span className='operation'>...</span>}
                                    </Dropdown>}
                                </Card>
                            </a>)
                        }
                    </Space>
                </div>)
            }

        </>
    )
}

export default Home
