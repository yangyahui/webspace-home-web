/*
 * @Author: yangyahui
 * @Date: 2022-08-08 18:24:15
 * @LastEditors: yangyahui
 * @LastEditTime: 2022-08-10 16:13:48
 * @Description: 请填写简介
 */
import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import path  from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh()],
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true

      }
    }
  },
  resolve:{
    alias:{
      "@":path.resolve(__dirname,"src"),
      "service":path.resolve(__dirname,"src/service"),
    }
  },
  server: {
    proxy: {
      // 字符串简写写法
      // '/foo': 'http://localhost:4567',
      // 选项写法
      '/api': {
        target: 'http://127.0.0.1:7001/',
        changeOrigin: true,
        // rewrite: (path) => path.replace(/^\/api/, 'api')
      },
      // // 正则表达式写法
      // '^/fallback/.*': {
      //   target: 'http://jsonplaceholder.typicode.com',
      //   changeOrigin: true,
      //   rewrite: (path) => path.replace(/^\/fallback/, '')
      // },
      // 使用 proxy 实例
      // '/api': {
      //   target: 'http://jsonplaceholder.typicode.com',
      //   changeOrigin: true,
      //   configure: (proxy, options) => {
      //     // proxy 是 'http-proxy' 的实例
      //   },
      // }
    }
  }
})
